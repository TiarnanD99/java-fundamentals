public class TestAccount {

    public static void main(String[] args) {
        Account myAccount = new Account();
        Account[] arrayOfAccounts = new Account[5];

        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};


        myAccount.setName("Tiarnan");
        myAccount.setBalance(100);

        System.out.println("my name is " + myAccount.getName());
        System.out.println("my account balance is " + myAccount.getBalance());

        myAccount.addInterest();
        System.out.println("my account balance after interest is " + myAccount.getBalance());

        for(int i = 0; i <5; i++){
           // arrayOfAccounts[i] = new Account(amounts[i], names[i]);
        }

    }
}
