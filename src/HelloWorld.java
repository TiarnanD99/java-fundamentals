public class HelloWorld {

    public static void main(String[] args){
        String make = "Renault";
        String model = "Laguna";
        double engineSize = 1.8;
        byte gear = 2;
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        if(engineSize <= 1.3){
            System.out.println("Car is weak");
        } else {
            System.out.println("Car is strong");
        }

        int myArr[] = new int[10];
        int count = 0;
        for(int i =1900; i<= 2000; i++ ){

            if(i%4 == 0 && count < 10){
                myArr[count] = i;
                count++;
            }
        }

        for(int leapYear: myArr){
            System.out.println(leapYear);
        }

    }

}
